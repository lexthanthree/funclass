(ns week1.core
  (:gen-class)
  (:require [clojure.math.numeric-tower :as math]))


(defn good_enough [guess x]
  (-> guess
      (math/expt 2)
      (- x)
      math/abs
      (< 0.0001)))

(defn improve [x guess]
  (-> (/ x guess)
      (+ guess)
      (/ 2.0)))

(defn sqrt [x]
  (let [imp (partial improve x)]
    (loop [x x guess 1.0]
      (if (good_enough guess x)
        guess
        (recur x (imp guess))))))

(defn pascal [col row]
  (let [recurse (fn [new-col] (pascal new-col (dec row)))]
    (cond
      (zero? col) 1                                         ; first col is always 1
      (= col row) 1                                         ; last col is always 1
      :else (+ (recurse (dec col))
               (recurse col)))))


(defn change [money coins]
  (let [change-maker (fn change-maker [cur-total other-coins]
                       (cond
                         (= money cur-total) 1
                         (or (< money cur-total) (empty? other-coins)) 0
                         :else (+ (change-maker (+ cur-total (first other-coins)) other-coins)
                                  (change-maker cur-total (rest other-coins)))))]
    (change-maker 0 coins)))

(defn balanced [parse-string]
  (let [balanced (fn balanced [opened rest-parse-string]
                   (let [recurse (fn [new-opened] (balanced new-opened (rest rest-parse-string)))]
                     (cond
                       (or (< opened 0) (empty? rest-parse-string)) opened
                       (= (first rest-parse-string) \() (recurse (inc opened))
                       (= (first rest-parse-string) \)) (recurse (dec opened))
                       :else (recurse opened))))]
    (= (balanced 0 parse-string) 0)))



(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "sqrt 5:" (sqrt 5))
  (println "pascal c2 r4" (pascal 2 4))
  (println "balanced :-)" (balanced ":-)"))
  (println "balanced (()))(" (balanced "(()))("))
  (println "balanced (thing(other())last)" (balanced "(thing(other())last)"))
  (println "change 4 [1, 2]" (change 4 [1, 2])))
