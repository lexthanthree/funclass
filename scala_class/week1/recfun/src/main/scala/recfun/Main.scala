package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
    * Exercise 1
    */
  def pascal(c: Int, r: Int): Int = {
    if (c == 0 || c == r) 1
    else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
    * Exercise 2
    */
  def balance(chars: List[Char]): Boolean = {
    def balance(count: Int, chars: List[Char]): Int = {
      if (chars.isEmpty || count < 0) count
      else if (chars.head == '(') balance(count + 1, chars.tail)
      else if (chars.head == ')') balance(count - 1, chars.tail)
      else balance(count, chars.tail)
    }

    balance(0, chars) == 0
  }

  /**
    * Exercise 3
    */
  def countChange(money: Int, coins: List[Int]): Int = {
    def countChange(curTotal: Int, otherCoins: List[Int]): Int = {
      if (otherCoins.isEmpty || curTotal > money) 0
      else if (curTotal + otherCoins.head == money) 1 + countChange(curTotal, otherCoins.tail)
      else countChange(curTotal + otherCoins.head, otherCoins) + countChange(curTotal, otherCoins.tail)
    }

    countChange(0, coins)
  }
}
